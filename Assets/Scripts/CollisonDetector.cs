﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisonDetector : MonoBehaviour {

	public Rigidbody2D obstacle;
	public Rigidbody2D obstacle1;
	public Rigidbody2D topObject;
	public Rigidbody2D bottomObject;
	public Camera mainCam = new Camera ();

	public Text gameOverText;
	// Use this for initialization
	void Start () {
		gameOverText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision)
	{
		Debug.Log("Collision Detected ");
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		gameOverText.text = "Game Over";
//		obstacle.constraints = RigidbodyConstraints2D.FreezeAll;
//		obstacle1.constraints = RigidbodyConstraints2D.FreezeAll;
		//Destroy(topObject);
		//Destroy (bottomObject);
		topObject.constraints = RigidbodyConstraints2D.FreezeAll;
		bottomObject.constraints = RigidbodyConstraints2D.FreezeAll;
//		Debug.Log ("Collision DetectedCD!!!!");
//		Destroy (other);
//		Debug.Log("Collision Detected "+other.gameObject.name );
//		if (other.gameObject.tag == "Player") {
//			Destroy (other);
//		}
	}
}
