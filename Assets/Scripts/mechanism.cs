﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mechanism : MonoBehaviour {

	public Rigidbody2D topObject;
	public Rigidbody2D bottomObject;
	//private int direction = 1;
	private bool goingUp = true;
	private float touchDuration = 0.0f;
	private Touch touch;
	//private int Score=0;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("callAtpointOne", 0.1f, 0.1f);
	}

	void callAtpointOne(){
		//if (Input.GetKeyDown(KeyCode.Space)) {
		if (Input.touchCount > 0 && Input.GetTouch(0).phase==TouchPhase.Stationary && topObject!=null && Input.GetTouch(0).tapCount==1) {
			//print (Input.touchCount+" ");
			//ToastMessage.print (Input.touchCount);
			goingUp = !goingUp;
			float force = 500f;
			if(!goingUp)
				force = -500f;
			topObject.velocity = new Vector2(0,0);
			bottomObject.velocity = new Vector2 (0, 0);
			Vector2 movement = new Vector2 (0.0f, 1.0f) ;
			topObject.AddForce(force*movement);
			bottomObject.AddForce (-force*movement);
		}
	}
	// Update is called once per frame
	void Update () {
//		if (Input.touchCount > 0 && Input.GetTouch(0).phase==TouchPhase.Stationary && topObject!=null && Input.GetTouch(0).tapCount==1) {
//			//print (Input.touchCount+" ");
//			//ToastMessage.print (Input.touchCount);
//			goingUp = !goingUp;
//			float force = 500f;
//			if(!goingUp)
//				force = -500f;
//			topObject.velocity = new Vector2(0,0);
//			bottomObject.velocity = new Vector2 (0, 0);
//			Vector2 movement = new Vector2 (0.0f, 1.0f) ;
//			topObject.AddForce(force*movement);
//			bottomObject.AddForce (-force*movement);
//		}
	}

	void OnCollisionEnter(Collision collision)
	{
		Debug.Log("Collision Detected ");
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		Debug.Log ("Collision Detected!!!!");
		Debug.Log("Collision Detected "+other.gameObject.name);
		if (other.gameObject.tag == "Player") {
			Destroy (other);
		}
	}
}
