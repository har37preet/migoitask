﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	private Rigidbody2D rb2d;
	private bool direction = false; // true-up, false-bottom
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
	}

	void LateUpdate () {
		if (Input.touchCount > 0 && Input.GetTouch(0).phase==TouchPhase.Stationary) {
			if (direction) {
				//Vector2 movement = new Vector2 (0.0f, 10.0f) ;
				direction = false;
				print (direction);
				//transform.position = Vector2.Lerp (transform.position, transform.forward, Time.deltaTime);
				rb2d.AddRelativeForce (Random.insideUnitCircle*1000);
				rb2d.velocity = Vector2.ClampMagnitude(rb2d.velocity, 10000);
			} else {
				//Vector2 movement = new Vector2 (0.0f, -10.0f);
				direction = true;
				print (direction);
				rb2d.AddRelativeForce (Random.insideUnitCircle*1000);
				//rb2d.AddForce ((rb2d.velocity.magnitude==0?1:rb2d.velocity.magnitude)*movement * 1000);
				//transform.position = Vector2.Lerp (transform.position, -transform.forward, Time.deltaTime);
			}
		}
	}
}
