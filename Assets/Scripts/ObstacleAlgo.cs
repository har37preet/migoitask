﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAlgo : MonoBehaviour {

	//public Sprite obstacleT;
	//public Sprite obstacleB;
	//public GameObject up, botton;
	public Rigidbody2D obstacle;
	public Rigidbody2D obstacle1;
	public Rigidbody2D topObject;
	public Rigidbody2D bottomObject;
	public Camera mainCam = new Camera ();
	// Use this for initialization
	void Start () {
		//up = new GameObject("obstacle");
		//SpriteRenderer renderer = up.AddComponent<SpriteRenderer>();
		//renderer.sprite = obstacleT;

		obstacle.position = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 4, 0f)).y);
		obstacle1.position = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x, mainCam.ScreenToWorldPoint (new Vector3 (0f, 3*Screen.height / 4, 0f)).y);
		InvokeRepeating ("createObstacles", 1.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		//up.GetComponent<Rigidbody2D>().velocity = new Vector2 (1.0f, 0.0f)*5;
		//obstacle.AddForce (new Vector2 (1.0f, 0.0f)*10);
		obstacle.velocity = new Vector2 (1.0f, 0.0f)*5;
		obstacle1.velocity = new Vector2 (1.0f, 0.0f)*5;
		//Instantiate (obstacle);

		GameObject[] obstacles =  GameObject.FindGameObjectsWithTag ("obstacle");

		for (int i = 2; i < obstacles.Length; i++) {
			//Rigidbody temp = obstacles [i].GetComponent<Rigidbody>();
			if(obstacles [i].transform.position.x <new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 4, 0f)).y).x || obstacles [i].transform.position.x >new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x, mainCam.ScreenToWorldPoint (new Vector3 (0f, 3*Screen.height / 4, 0f)).y).x){
				Destroy (obstacles[i]);
			}
		}
	}

	void createObstacles(){
		Rigidbody2D obs =  Instantiate(obstacle);
		//obs = obstacle;
		obs.transform.localScale = new Vector2 (Random.Range(0.3f,0.5f),0.10f);
		obs.position = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height / 4, 0f)).y);
		obs.velocity = new Vector2 (1.0f, 0.0f)*5;

		Rigidbody2D obs1 =  Instantiate(obstacle1);
		//obs = obstacle;
		obs1.transform.localScale = new Vector2 (Random.Range(0.3f,0.5f),0.10f);
		obs1.position = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x, mainCam.ScreenToWorldPoint (new Vector3 (0f, 3*Screen.height / 4, 0f)).y);
		obs1.velocity = new Vector2 (-1.0f, 0.0f)*5;
		//obs.AddForce (new Vector2 (1.0f, 0.0f)*10);
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		print ("Collision Detected!!!!");
		Debug.Log("Collision Detected "+other.gameObject.name);
		if (other.gameObject.tag == "Player") {
			Destroy (other);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		Destroy (collision.gameObject);
		Debug.Log("Collision Detected ");
	}
}
