﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateBoundry : MonoBehaviour {
	
	BoxCollider2D topWall;
	BoxCollider2D bottomWall;
	BoxCollider2D leftWall;
	BoxCollider2D rightWall;
	BoxCollider2D middleWall;
	public Rigidbody2D topBall;
	public Rigidbody2D bottomBall;

	public Camera mainCam = new Camera();
	// Use this for initialization
	void Start () {
		topWall = (BoxCollider2D)gameObject.AddComponent<BoxCollider2D> ();
		bottomWall = (BoxCollider2D)gameObject.AddComponent<BoxCollider2D> ();
		middleWall = (BoxCollider2D)gameObject.AddComponent<BoxCollider2D> ();

		//leftWall = (BoxCollider2D)gameObject.AddComponent<BoxCollider2D> ();
		//rightWall = (BoxCollider2D)gameObject.AddComponent<BoxCollider2D> ();

		topWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2.5f, 0f, 0f)).x, 1f);
		topWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y + 0.1f);
		topWall.tag = "wall";

		bottomWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2.5f, 0f, 0f)).x, 1f);
		bottomWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).y - 0.5f);
		bottomWall.tag = "wall";

		middleWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2.5f, 0f, 0f)).x, 0.1f);
		middleWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height/2, 0f)).y);
		//print (middleWall.offset);

		topBall.position = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y - 0.5f);
		bottomBall.position = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).y + 0.5f);



		//topWall.isTrigger = true;
		//bottomWall.isTrigger = true;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
